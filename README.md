SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

T. Fetzer, c. Grueninger, B. Flemisch, and R. Helmig<br>
[On the conditions for coupling free and porous-medium flow in a finite volume framework]
(http://dx.doi.org/10.1007/978-3-319-57394-6_37)<br>
Finite Volumes for Complex Applications VIII - Hyperbolic, Elliptic and Parabolic Problems: FVCA 8, Lille, France, June 2017<br>
DOI: 10.1007/978-3-319-57394-6_37

You can use the .bib file provided [here](Fetzer2017a.bib).


Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installFetzer2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017a/raw/master/installFetzer2017a.sh)
in this folder.

```bash
mkdir -p Fetzer2017a && cd Fetzer2017a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017a/raw/master/installFetzer2017a.sh
sh ./installFetzer2017a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The applications used for this publication can be found in
appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/.
This folder also contains the jobfiles which specify the individual setups.
There is a test which can be run using the ctest facility to check whether the reference
results are obtained.

* __windtunnel__:
  The basic setup of flow in a windtunnel which is disturbed by a porous medium block

In order to compile the executable, type:
```bash
cd dumux-Fetzer2017a/build-cmake/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/
make
ctest
./windtunnel test.input
```

Then you can either run the executable with any input file
```bash
./windtunnel test.input
```

Or you can start the testing enivronment and check whether you obtain the same results for a simple test case
```bash
ctest
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installFetzer2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017a/raw/master/installFetzer2017a.sh).
This publication bases on a specfic commit in dumux-2.11-git, however it may work and produces similar results
for dumux-2.11.

In addition the following external software packages, with one of the two linear
solvers, are necessary for compiling the executables:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              | 3.4.3   | build tool        |
| suitesparse        | 4.4.1   | matrix algorithms |
| UMFPack            | 5.7.1   | linear solver     |
| SuperLU            | 4.3     | linear solver     |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ | 3.5.0   |
| gcc/g++       | 6.2.1   |
