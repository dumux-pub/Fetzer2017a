#!/bin/sh

### Create a folder for the DUNE and DuMuX modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone git://github.com/smuething/dune-multidomain.git
git clone git://github.com/smuething/dune-multidomaingrid.git
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2017a.git dumux-Fetzer2017a

### Go to specific branches
cd dune-common && git checkout releases/2.4 && cd ..
cd dune-geometry && git checkout releases/2.4 && cd ..
cd dune-grid && git checkout releases/2.4 && cd ..
cd dune-istl && git checkout releases/2.4 && cd ..
cd dune-localfunctions && git checkout releases/2.4 && cd ..
cd dune-multidomain && git checkout releases/2.0 && cd ..
cd dune-multidomaingrid && git checkout releases/2.3 && cd ..
cd dune-pdelab && git checkout releases/2.0 && cd ..
cd dune-typetree && git checkout releases/2.3 && cd ..
cd dumux && git checkout master && cd ..
# cd dumux-Fetzer2017a && git checkout master && cd ..

### Go to specific commits
cd dune-common && git checkout e1a9b914d0a3b133641647a6987c61c9e2a5423a && cd ..
cd dune-geometry && git checkout ac1fca4ff249ccdc7fb035fa069853d84b93fb73 && cd ..
cd dune-grid && git checkout 5aeced8b0a64d46ff12afd3a252f99c19a8575d8 && cd ..
cd dune-istl && git checkout 374b7b301bcfb8efbc15b7d93a79fed7dc1ad3ac && cd ..
cd dune-localfunctions && git checkout b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a && cd ..
cd dune-multidomain && git checkout e3d52982dc9acca9bf13cd8f77bf0329c61b6327 && cd ..
cd dune-multidomaingrid && git checkout 3b829b7a130473749b8af2d402eaef1eff1071a7 && cd ..
cd dune-pdelab && git checkout 19c782eea7232e94849617b20dfee8d9781eb4fb && cd ..
cd dune-typetree && git checkout ecffa10c59fa61a0071e7c788899464b0268719f && cd ..
cd dumux && git checkout d766e80db06fcae96ed57ea9d7e3d205743a4098 && cd ..
# cd dumux-Fetzer2017a && git checkout master && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux-Fetzer2017a/gcc-optim.opts all
./dune-common/bin/dunecontrol --opts=dumux-Fetzer2017a/clang-optim.opts all
