
install(FILES
        eddyviscosity2cniindices.hh
        eddyviscosity2cniproperties.hh
        eddyviscosity2cnipropertydefaults.hh
        eddyviscosityindices.hh
        eddyviscosityproperties.hh
        eddyviscositypropertydefaults.hh
        eddyviscositystaggeredgrid.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/freeflow/eddyviscosity2cni)
