
install(FILES
        basezeroeqstaggeredgrid.hh
        zeroeqindices.hh
        zeroeqproperties.hh
        zeroeqpropertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/freeflow/zeroeq/zeroeq)
