/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Base class for transient part of staggered grid discretization for
 *        Navier-Stokes equation.
 *
 * The Navier-Stokes-Equations:<br>
 *
 * Mass balance:
 * \f[
 *    \frac{\partial}{\partial t} \varrho
 *    + \dots
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial}{\partial t} \left( \varrho v \right)
 *    + \dots
 *    = 0
 * \f]
 */

#ifndef DUMUX_BASE_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_DUMUX_HH
#define DUMUX_BASE_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_DUMUX_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include "basenavierstokesstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator base class for staggered grid discretization solving
     * the transient part of the Navier-Stokes equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseNavierStokesTransientStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, TransientLocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

      //! \brief Index of unknowns
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      BaseNavierStokesTransientStaggeredGrid(GridView gridView_)
        : gridView(gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // This function is empty on purpose. From the child classes this function
        // has to call the respective CNI version below.
      }

      /**
       * \copydoc BaseNavierStokesTransientStaggeredGrid::alpha_volume
       * \param velocity phase velocity vector of the velocity on the inside the element faces
       * \param pressure phase pressure inside the element
       * \param massMoleFrac phase composition inside the element
       * \param temperature phase temperature inside the element
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_massmomentum(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                     std::vector<DimVector> velocityFaces, Scalar pressure,
                                     Scalar massMoleFrac, Scalar temperature) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        //! \todo: an other option is to introduce a function which completes the fluid,
        //!        because all information (p,T,x) is given here and than directly
        //!        call the FluidSystem or State
        const Scalar density = asImp_().density(pressure, temperature, massMoleFrac);
        const Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Storage term of \b mass balance equation
          * \f[
          *    \frac{\partial}{\partial t} varrho
          * \f].
          */
        r.accumulate(lfsu_p, 0,
                      density * elementVolume);

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (2) \b Storage term of \b momentum balance equation
           * \f[
           *    \frac{\partial}{\partial t} \left( \varrho v \right)
           * \f].
           */
          r.accumulate(lfsu_v, 2*curDim,
                       density
                       * velocityFaces[curDim*2][curDim]
                       * elementVolume * 0.5); // half cell volume, other half for other volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       density
                       * velocityFaces[curDim*2+1][curDim]
                       * elementVolume * 0.5); // half cell volume, other half for other volume
        }
      }

      /**
       * \brief Returns the velocities for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const std::vector<DimVector> velocity (const EG& eg, const LFSU& lfsu, const X& x) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
        }

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v.size());
          lfsu_v.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // evaluate velocity on intersection
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v.size(); i++)
          {
            velocityFaces[curFace].axpy(x(lfsu_v, i), velocityBasis[curFace][i]);
          }
        }

        return velocityFaces;
      }

      /**
       * \brief Returns the pressure for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar pressure(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();
        return x(lfsu_p, 0);
      }

private:
      GridView gridView;
      //! Instationary variables
      double time;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_BASE_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_DUMUX_HH
