// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \brief Newton method with overwritten execute_ method to include
 *        local operators from PDELab.
 */

#ifndef DUMUX_MULTIDOMAIN_NEWTONMETHOD_HH
#define DUMUX_MULTIDOMAIN_NEWTONMETHOD_HH

#include <dune/istl/umfpack.hh>

#include <dune/pdelab/backend/istl/utility.hh>
#include <dune/pdelab/gridoperator/common/timesteppingparameterinterface.hh>

#include <dumux/nonlinear/newtonmethod.hh>

// #ifdef ITERATIVE_LINEAR_SOLVE
// #include "iterativesolve.hh"
// #endif
#include "multidomainproperties.hh"

namespace Dumux {

namespace Properties
{
NEW_PROP_TAG(NewtonResidualReduction);
NEW_PROP_TAG(NewtonMaxRelativeShift);
NEW_PROP_TAG(NewtonMaxSteps);
}

/*!
 * \ingroup MultidomainModel
 * \brief Newton method with overwritten execute_ method to include
 *        local operators from PDELab.
 */
template<class TypeTag>
class PDELabNewtonMethod
    : public NewtonMethod<typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag)>
{
    using ParentType = NewtonMethod<typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag)>;

    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace) MultiDomainGridFunctionSpace;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator) MultiDomainGridOperator;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, LinearSolver) LinearSolver;

    typedef typename GET_PROP_TYPE(DarcySubProblemTypeTag, NewtonController) NewtonController;
    typedef typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem) Problem;

    // type of coefficient vector, used for solution and residual
    using Vector = typename MultiDomainGridOperator::Traits::Domain;
    using Matrix = typename MultiDomainGridOperator::Traits::Jacobian;
    using MatrixBase = typename Matrix::BaseT;

public:
   /*!
    * \brief Constructor
    */
    PDELabNewtonMethod(Problem &problem)
      : ParentType(problem)
    {
        useLineSearch_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, UseLineSearch);
        writeNewtonConvergence_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, WriteConvergence);
        maxNewtonSteps_ = GET_PARAM_FROM_GROUP(TypeTag, int, Newton, MaxSteps);
        resReduction_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, ResidualReduction);
        maxRelShift_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, MaxRelativeShift);
    }

    /*!
     * \brief Pass variables needed to synchronize the solutions between
     *        DuMuX and PDELab local operators.
     */
    void passAdditionalVariables(std::shared_ptr<MultiDomainGridOperator> gridOperatorPdelab,
                                 std::shared_ptr<MultiDomainGridFunctionSpace> gridFunctionSpacePdelab,
                                 std::function<void (Vector&, SolutionVector&, bool)> copyVectorPdelabToDumux,
                                 std::function<void (Vector&)> updateConstraints,
                                 std::function<void (Vector&, bool&, bool)> updateStoredVariables,
                                 std::function<void (Vector&, unsigned int, bool)> writeVtkOutput,
                                 Vector initialSolution)
    {
        gridOperatorPdelab_ = gridOperatorPdelab;
        gridFunctionSpacePdelab_ = gridFunctionSpacePdelab;
        copyVectorPdelabToDumux_ = copyVectorPdelabToDumux;
        updateConstraints_ = updateConstraints;
        updateStoredVariables_ = updateStoredVariables;
        writeVtkOutput_ = writeVtkOutput;
        currentSolution_ = std::make_shared<Vector>(initialSolution);
    }

    /*!
     * \brief Run the newton method. The controller is responsible
     *        for all the strategic decisions.
     *
     * \note This method is an exact copy of NewtonController::execute()
     *       and is only used to hide the original function with it's
     *       call to execute_().
     */
    bool execute(NewtonController &ctl)
    {
        ctl.setVerbose(false);
        try
        {
            return execute_(ctl);
        }
        catch (const Dumux::NumericalProblem &e)
        {
            if (ctl.verbose())
            {
                std::cout << "Newton: Caught exception: \"" << e.what() << "\"\n";
            }
            return false;
        }
    }

protected:
    // inherit constructor
    using NewtonMethod<typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag)>::NewtonMethod;

    /*!
     * \note This method additionally copies the solution
     *       in sync between the PDELab and DuMuX data
     *       structures.
     */
    bool execute_(NewtonController &ctl)
    {
        Dune::Timer assembleTimer(false);
        Dune::Timer solveTimer(false);
        Dune::Timer updateTimer(false);

        assembleTimer.start();
        // solution u from the current solution
        SolutionVector& uCurrentIter = this->model().curSol();
        Vector solution(*gridFunctionSpacePdelab_, 0.0);
        Vector residual(*gridFunctionSpacePdelab_, 0.0);

        // prepare PDELab grid operator, use implicit Euler scheme
        Dune::PDELab::ImplicitEulerParameter<Scalar> implicitEuler;
        gridOperatorPdelab_->preStep(implicitEuler,
                                     this->problem().timeManager().time(),
                                     this->problem().timeManager().timeStepSize());

        solution = *currentSolution_;
        updateConstraints_(solution);

        std::vector<Vector*> x(1); // vector of pointers to all steps
        x[0] = &solution;
        gridOperatorPdelab_->preStage(1, x);

        // represent operator as a matrix
        Matrix m(*gridOperatorPdelab_);

        // evaluate residual for initial condition
        Vector r(*gridFunctionSpacePdelab_, 0.0);
        gridOperatorPdelab_->residual(solution, r);

        unsigned int verbose = 4;
        Scalar defectInitial = r.two_norm();
        Scalar lastDefect = defectInitial;
        Scalar reduction = 1.0;
        if (verbose >= 4)
        {
            std::cout << "   initial defect=" << defectInitial << std::endl;
        }
        assembleTimer.stop();

        ctl.newtonBegin(*this, uCurrentIter/* not used */);
        for (unsigned int newtonStep = 1; newtonStep < maxNewtonSteps_ + 1; newtonStep++)
        {
            m = 0.0;
            assembleTimer.start();
            gridOperatorPdelab_->jacobian(solution, m);
            assembleTimer.stop();

            // solve the jacobian system
            solveTimer.start();
            Vector update(*gridFunctionSpacePdelab_, 0.0);
#ifdef ITERATIVE_LINEAR_SOLVE
            iterativeSolve(Dune::PDELab::istl::raw(m),
                           Dune::PDELab::istl::raw(update),
                           Dune::PDELab::istl::raw(r));
#else
            LinearSolver solver(Dune::PDELab::istl::raw(m), false);
            Dune::InverseOperatorResult stat;

            solver.apply(update, r, stat);
#endif
            solveTimer.stop();

            // line search
            Vector oldSolution = solution;
            Scalar lambda = 1.0;
            Scalar defect = 0.0;
            for (unsigned int lineSearchStep = 0; lineSearchStep <= maxLineSearchSteps_; ++lineSearchStep)
            {
                // apply the update the current solution and copy it to Dumux' Darcy model
                updateTimer.start();
                solution.axpy(-lambda, update);
                copyVectorPdelabToDumux_(solution, uCurrentIter, false);
                updateTimer.stop();

                // write intermediate solution for every Newton step, similar to Newton convergence writer
                if (writeNewtonConvergence_)
                {
                    writeVtkOutput_(solution, newtonStep, false);
                }

                assembleTimer.start();
                r = 0.0;
                gridOperatorPdelab_->residual(solution, r);
                defect = r.two_norm();
                if (useLineSearch_
                    && defect > (1.0 - lambda * 0.25) * lastDefect
                    && lineSearchStep < maxLineSearchSteps_)
                {
                    solution = oldSolution;
                    lambda *= 0.5;
                }
                else
                {
                    break;
                }
                assembleTimer.stop();
            }

            updateTimer.start();
            reduction = defect / lastDefect;
            lastDefect = defect;

            // copy current solution to Dumux' Darcy model and  update static data to consider phase switches
#if !TWOP_COUPLING
            this->problem().model().updateStaticData(uCurrentIter, uCurrentIter /* not used */);
#endif
            copyVectorPdelabToDumux_(solution, uCurrentIter, true /* reverse */);

            // update the stored variables and apply the slope limiter on k, epsilon if necessary
            updateTimer.start();
            bool acceptSolution = true;
            updateStoredVariables_(solution, acceptSolution, false);
            updateTimer.stop();

            ctl.newtonEndStep(uCurrentIter, uCurrentIter /*both not used */);
            updateTimer.stop();

            Scalar maxRelShift = 0.0;
            for (unsigned int i = 0; i < solution.N(); ++i)
            {
                Scalar eqErr = std::abs(solution.block(i) - oldSolution.block(i));
                eqErr /= std::max<Scalar>(1.0, std::abs(solution.block(i) + oldSolution.block(i))/2);
                maxRelShift = std::max(maxRelShift, eqErr);
            }

            if (verbose >= 4)
            {
                std::cout << "   Newton iteration " << newtonStep
                          << " done, defect=" << defect
                          << ", resReduction=" << defect/defectInitial
                          << ", maxRelShift=" << maxRelShift
                          << std::endl;
            }

            if ((defect < resReduction_*defectInitial || maxRelShift < maxRelShift_)
#if !TWOP_COUPLING
                && !this->problem().model().switched()
#endif
                && newtonStep >= 2
                && acceptSolution)
            {
                updateTimer.start();
                updateStoredVariables_(solution, acceptSolution, true);
                updateTimer.stop();
                if (verbose >= 5)
                {
                    std::cout << "Newton iteration quit because no further improvement needed. Time:"
                      << (this->problem().timeManager().time() + this->problem().timeManager().timeStepSize())
                      << " time step: " << this->problem().timeManager().timeStepIndex() << std::endl;
                }
                break;
            }

            if (newtonStep == maxNewtonSteps_)
            {
                if (!acceptSolution)
                {
                    std::cout << "Update failed. Negative values for k or epsilon. Redoing time step with smaller time step size." << std::endl;
                }
                return false;
            }

            if (isnan(defect))
            {
                DUNE_THROW(Dumux::NumericalProblem, "defect is NaN.");
            }
        }

        // update the solution
        currentSolution_ = std::make_shared<Vector>(solution);

        if (verbose >= 4) {
            Scalar elapsedTot = assembleTimer.elapsed() + solveTimer.elapsed() + updateTimer.elapsed();
            std::cout << "Assemble/solve/update time: "
                      <<  assembleTimer.elapsed() << "(" << 100*assembleTimer.elapsed()/elapsedTot << "%)/"
                      <<  solveTimer.elapsed() << "(" << 100*solveTimer.elapsed()/elapsedTot << "%)/"
                      <<  updateTimer.elapsed() << "(" << 100*updateTimer.elapsed()/elapsedTot << "%)"
                      << "\n";
        }

        // write Vtk output
        writeVtkOutput_(solution, this->problem().timeManager().timeStepIndex() + 1, true);

        return true;
    }

private:
    std::shared_ptr<MultiDomainGridOperator> gridOperatorPdelab_ = nullptr;
    std::shared_ptr<MultiDomainGridFunctionSpace> gridFunctionSpacePdelab_ = nullptr;
    std::shared_ptr<Vector> currentSolution_ = nullptr;
    std::function<void (Vector&, SolutionVector&, bool)> copyVectorPdelabToDumux_;
    std::function<void (Vector&)> updateConstraints_;
    std::function<void (Vector&, bool&, bool)> updateStoredVariables_;
    std::function<void (Vector&, unsigned int, bool)> writeVtkOutput_;

    bool useLineSearch_;
    bool writeNewtonConvergence_;
    unsigned int maxNewtonSteps_;
    Scalar resReduction_;
    Scalar maxRelShift_;

    constexpr static unsigned int maxLineSearchSteps_ = 8;
};

} // namespace Dumux

#endif // DUMUX_MULTIDOMAIN_NEWTONMETHOD_HH
