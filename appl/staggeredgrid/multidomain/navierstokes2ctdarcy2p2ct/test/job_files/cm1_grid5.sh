#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
builddir=/temp/fetzer/dumux-211/dumux-Fetzer2017a/build-clang/
outdir=$builddir/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/results

# predefined names
executable=windtunnel
input=coupling.input
sourcedir=$builddir/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test
simdir=$outdir/cm1/grid5

# make executable
cd $sourcedir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Coupling.Method 1 \
  -Grid.Refinement 5 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
