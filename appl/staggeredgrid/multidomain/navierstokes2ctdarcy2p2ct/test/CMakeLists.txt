add_input_file_links()
dune_symlink_to_source_files(FILES test_references job_files)

# test: cm1
add_dumux_test(cm1 windtunnel windtunnel.cc
  python ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
    --script fuzzy
    --command "${CMAKE_CURRENT_BINARY_DIR}/windtunnel test.input -Problem.Name cm1_test -Coupling.Method 1"
    --files ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm1_test-ff.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm1_test-ff-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm1_test-ffSecondary.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm1_test-ffSecondary-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm1_test-pm.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm1_test-pm-00002.vtu
  )
target_compile_definitions(windtunnel PUBLIC "COUPLING_ZEROEQ=1")

# test: cm2
add_dumux_test(cm2 windtunnel windtunnel.cc
  python ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
    --script fuzzy
    --command "${CMAKE_CURRENT_BINARY_DIR}/windtunnel test.input -Problem.Name cm2_test -Coupling.Method 2"
    --files ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm2_test-ff.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm2_test-ff-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm2_test-ffSecondary.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm2_test-ffSecondary-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm2_test-pm.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm2_test-pm-00002.vtu
  )
target_compile_definitions(windtunnel PUBLIC "COUPLING_ZEROEQ=1")

# test: cm3
add_dumux_test(cm3 windtunnel windtunnel.cc
  python ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
    --script fuzzy
    --command "${CMAKE_CURRENT_BINARY_DIR}/windtunnel test.input -Problem.Name cm3_test -Coupling.Method 3"
    --files ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm3_test-ff.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm3_test-ff-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm3_test-ffSecondary.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm3_test-ffSecondary-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm3_test-pm.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm3_test-pm-00002.vtu
  )
target_compile_definitions(windtunnel PUBLIC "COUPLING_ZEROEQ=1")

# test: cm4
add_dumux_test(cm4 windtunnel windtunnel.cc
  python ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
    --script fuzzy
    --command "${CMAKE_CURRENT_BINARY_DIR}/windtunnel test.input -Problem.Name cm4_test -Coupling.Method 4"
    --files ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm4_test-ff.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm4_test-ff-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm4_test-ffSecondary.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm4_test-ffSecondary-00002.vtu
            ${CMAKE_CURRENT_SOURCE_DIR}/test_references/cm4_test-pm.vtu
            ${CMAKE_CURRENT_BINARY_DIR}/cm4_test-pm-00002.vtu
  )
target_compile_definitions(windtunnel PUBLIC "COUPLING_ZEROEQ=1")

install(FILES
        windtunnel.cc
        windtunneldarcyspatialparams.hh
        windtunneldarcysubproblem.hh
        windtunnelproblem.hh
        windtunnelstokessubproblem.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test)
