add_subdirectory("test")

install(FILES
        boundaryconditions.hh
        couplingoperator.hh
        indices.hh
        problem.hh
        properties.hh
        propertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct)
