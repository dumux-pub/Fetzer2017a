// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief \todo please doc me
 */
#ifndef DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_BOUNDARY_CONDITIONS_HH
#define DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_BOUNDARY_CONDITIONS_HH

#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/localoperator/diffusionparam.hh>

#include <dumux/porousmediumflow/2p2c/implicit/model.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

namespace Dumux
{

/*!
 * \brief Select boundary conditions type for Darcy unknown
 *
 * \tparam TypeTag Multi domain type tag
 * \tparam idx index for unkown
 */
template<typename TypeTag, unsigned int idx>
class DarcyBoundaryType
: public Dune::PDELab::BoundaryGridFunctionBase<
      Dune::PDELab::BoundaryGridFunctionTraits<
          typename GET_PROP_TYPE(TypeTag, MultiDomainGridView),
          Dune::PDELab::DiffusionBoundaryCondition::Type, 1,
          Dune::FieldVector<Dune::PDELab::DiffusionBoundaryCondition::Type, 1> >,
      DarcyBoundaryType<TypeTag, idx> >
{
    // Dumux types
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);
    using BoundaryTypes = typename GET_PROP_TYPE(DarcySubProblemTypeTag, BoundaryTypes);
    using Problem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem);

    MultiDomainGridView gv_;
    Problem& problem_;

public:
    // PDELab types and variables
    using BC = Dune::PDELab::DiffusionBoundaryCondition;
    using Traits = Dune::PDELab::BoundaryGridFunctionTraits<MultiDomainGridView,
        BC::Type, 1, Dune::FieldVector<BC::Type, 1> >;

    DarcyBoundaryType(Problem& problem)
    : gv_(problem.gridView()), problem_(problem)
    {}

    template<typename I>
    void evaluate(const Dune::PDELab::IntersectionGeometry<I>& ig,
                  const typename Traits::DomainType& x,
                  typename Traits::RangeType& y) const
    {
        const typename Traits::DomainType& globalPos = ig.geometry().global(x);

        BoundaryTypes boundaryTypes;
        problem_.boundaryTypesAtPos(boundaryTypes, globalPos);

        if (boundaryTypes.isDirichlet(idx))
        {
            y = BC::Dirichlet;
        }
        else
        {
            y = BC::Neumann;
        }
    }
};

/*!
 * \brief Boundary values for initial or Dirichlet condition for Darcy unknown
 *
 * \tparam TypeTag Multi domain type tag
 * \tparam idx index for unkown
 */
template<typename TypeTag, unsigned int idx>
class DarcyBoundaryValue
: public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<
          typename GET_PROP_TYPE(TypeTag, MultiDomainGridView),
          typename GET_PROP_TYPE(TypeTag, Scalar),
          1>,
      DarcyBoundaryValue<TypeTag, idx> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
    // Dumux types and variables
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);
    using PrimaryVariables = typename GET_PROP_TYPE(DarcySubProblemTypeTag, PrimaryVariables);
    using Problem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem);
    using Scalar = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Scalar);

    Problem& problem_;
public:
    // PDELab types
    using Traits = Dune::PDELab::AnalyticGridFunctionTraits<MultiDomainGridView, Scalar, 1>;
    using BaseT = Dune::PDELab::AnalyticGridFunctionBase<Traits, DarcyBoundaryValue<TypeTag, idx> >;

    DarcyBoundaryValue(Problem& problem)
    : BaseT(problem.gridView()), problem_(problem)
    {}

    void evaluateGlobal(const typename Traits::DomainType& globalPos,
                        typename Traits::RangeType& y) const
    {
        PrimaryVariables primaryVariables;
        problem_.dirichletAtPos(primaryVariables, globalPos);
        y = primaryVariables[idx];
    }
};

/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium. During
 *        buoyancy driven upward migration the gas passes a high
 *        temperature area.
 *
 */
template <class TypeTag>
class DarcyBoundaries
{
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);
    using Problem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem);
    using Indices = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Indices);
    // boundary type function types
    using PressureBT = DarcyBoundaryType<TypeTag, Indices::pressureIdx>;
    using SwitchBT = DarcyBoundaryType<TypeTag, Indices::switchIdx>;
    using TemperatureBT = DarcyBoundaryType<TypeTag, Indices::temperatureIdx>;
    // boundary value function types
    using PressureBV = DarcyBoundaryValue<TypeTag, Indices::pressureIdx>;
    using SwitchBV = DarcyBoundaryValue<TypeTag, Indices::switchIdx>;
    using TemperatureBV = DarcyBoundaryValue<TypeTag, Indices::temperatureIdx>;
    using BoundaryType =
        Dune::PDELab::CompositeConstraintsParameters<PressureBT, SwitchBT, TemperatureBT>;
public:
    using BoundaryValue =
        Dune::PDELab::CompositeGridFunction<PressureBV, SwitchBV, TemperatureBV>;

    /*!
     * \brief The constructor.
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    DarcyBoundaries(Problem& problem)
    : problem_(problem)
    , pressureBT_(problem)
    , switchBT_(problem)
    , temperatureBT_(problem)
    , pressureBV_(problem)
    , switchBV_(problem)
    , temperatureBV_(problem)
    , boundaryType(pressureBT_, switchBT_, temperatureBT_)
    , boundaryValue(pressureBV_, switchBV_, temperatureBV_)
    {}

private:
    Problem& problem_;
    PressureBT pressureBT_;
    SwitchBT switchBT_;
    TemperatureBT temperatureBT_;
    PressureBV pressureBV_;
    SwitchBV switchBV_;
    TemperatureBV temperatureBV_;
public:
    BoundaryType boundaryType;
    BoundaryValue boundaryValue;
};

} //end namespace

#endif // DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_BOUNDARY_CONDITIONS_HH
